web:
	python3 script.py

server: web
	git ls-files | entr make web &
	sleep 0.1 && firefox localhost:8000 &
	python3 -m http.server --bind localhost --directory public
