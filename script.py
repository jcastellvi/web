#!/usr/bin/env python3
import json
import os
from datetime import date

articles = ["limits_chordal_tw", "chordal_bounded_tw", "chordal_planar"]

conferencies = ["dmd_2024", "eurocomb_2023"]

xerrades = ["gazteak_2025", "dmd_2024", "eurocomb_2023", "journees_alea_2022"]



avui = date.today()
naixement = date(1998, 5, 29)



############
###CATALÀ###
############




with open("pre_index.html", "r") as pre_index:
    html = pre_index.read();


#Articles

articles_str = ""

for article in articles:
    with open("publicacions/" + article + ".json", "r") as fitxer_article:
        dades = json.loads(fitxer_article.read().replace('\\', '\\\\'))

    resum = dades["resum"]
    titol = dades["titol"]
    estat = dades["estat"] #preprint, submitted, accepted
    revista = dades["revista"]

    while "$" in resum:
        resum = resum.replace("$", "\\(", 1)
        resum = resum.replace("$", "\\)", 1)
    resum = resum.replace("\\n ", "\n</p>\n<p>\n")

    while "$" in titol:
        titol = titol.replace("$", "\\(", 1)
        titol = titol.replace("$", "\\)", 1)

    autors = ""
    if len(dades["autors"])>0:
        autors = "amb " + dades["autors"][0]["article"] + dades["autors"][0]["nom"] + " " + dades["autors"][0]["cognom"]
        for i in range(1, len(dades["autors"])):
            if i == len(dades["autors"])-1:
                autors = autors + " i " + dades["autors"][0]["article"] + dades["autors"][i]["nom"] + " " + dades["autors"][i]["cognom"]
            else:
                autors = autors + ", " + dades["autors"][0]["article"] + dades["autors"][i]["nom"] + " " + dades["autors"][i]["cognom"]

    articles_str = articles_str + "<p id=\"article_" + article + "\" class=\"entrada\">\n<b class=\"titol\">" + titol + "</b>,<br>\n" + autors + ".<br>\n"

    if estat=="preprint":
        articles_str = articles_str + "<a href=\"https://arxiv.org/abs/" + dades["arxiv"] + "\" target=\"_blank\" rel=\"noopener noreferrer\">arXiv:" + dades["arxiv"] + "</a>."
    if estat=="submitted":
        articles_str = articles_str + "<em>Sotmès.</em> <a href=\"https://arxiv.org/abs/" + dades["arxiv"] + "\" target=\"_blank\" rel=\"noopener noreferrer\">arXiv:" + dades["arxiv"] + "</a>."
    if estat=="accepted":
        articles_str = articles_str + "<em>" + revista + "</em> <a href=\"https://arxiv.org/abs/" + dades["arxiv"] + "\" target=\"_blank\" rel=\"noopener noreferrer\">arXiv:" + dades["arxiv"] + "</a>."
    
    if "conferencia" in dades:
        articles_str = articles_str + "<br>\n<a href=\"#conferencia_" + dades["conferencia"] + "\">Versió de conferència</a>."

    articles_str = articles_str + "\n</p>\n<details class=\"resum\">\n<summary>Resum</summary>\n<p>\n" + resum + "\n</p>\n</details>\n"
    
    


#Conferencies

conferencies_str = ""

for conferencia in conferencies:
    with open("conferencies/" + conferencia + ".json", "r") as fitxer_conferencia:
        dades = json.loads(fitxer_conferencia.read().replace('\\', '\\\\'))

    titol = dades["titol"]
    nom_conferencia = dades["conferencia"]

    if "resum" in dades:
        resum = dades["resum"]
        while "$" in resum:
            resum = resum.replace("$", "\\(", 1)
            resum = resum.replace("$", "\\)", 1)
        resum = resum.replace("\\n ", "\n</p>\n<p>\n")

    while "$" in titol:
        titol = titol.replace("$", "\\(", 1)
        titol = titol.replace("$", "\\)", 1)

    autors = ""
    if len(dades["autors"])>0:
        autors = "amb " + dades["autors"][0]["article"] + dades["autors"][0]["nom"] + " " + dades["autors"][0]["cognom"]
        for i in range(1, len(dades["autors"])):
            if i == len(dades["autors"])-1:
                autors = autors + " i " + dades["autors"][0]["article"] + dades["autors"][i]["nom"] + " " + dades["autors"][i]["cognom"]
            else:
                autors = autors + ", " + dades["autors"][0]["article"] + dades["autors"][i]["nom"] + " " + dades["autors"][i]["cognom"]

    conferencies_str = conferencies_str + "<p id=\"conferencia_" + conferencia + "\" class=\"entrada\">\n<b class=\"titol\">" + titol + "</b>,<br>\n" + autors + ".<br>\n<em>" + nom_conferencia + "</em><br>\n<a href=\"/conferencies/" + conferencia + ".pdf\" target=\"_blank\" rel=\"noopener noreferrer\">PDF</a>"
    
    if "acta" in dades:
        conferencies_str = conferencies_str + ",\n<a href=\"" + dades["acta"] + "\" target=\"_blank\" rel=\"noopener noreferrer\">acta</a>"
    
    if "article" in dades:
        conferencies_str = conferencies_str + ",\n<a href=\"#article_" + dades["article"] + "\">versió de revista</a>"
        
    conferencies_str = conferencies_str + ".\n</p>\n"
        
    if "resum" in dades:
        conferencies_str = conferencies_str + "<details class=\"resum\">\n<summary>Resum</summary>\n<p>\n" + resum + "\n</p>\n</details>\n"




#Xerrades

xerrades_str = ""

for xerrada in xerrades:
    with open("xerrades/" + xerrada + ".json", "r") as fitxer_xerrada:
        dades = json.loads(fitxer_xerrada.read().replace('\\', '\\\\'))

    titol = dades["titol"]
    data = dades["data"]
    esdeveniment = dades["esdeveniment"]
    lloc = dades["lloc_ca"]

    if "resum" in dades:
        resum = dades["resum"]
        while "$" in resum:
            resum = resum.replace("$", "\\(", 1)
            resum = resum.replace("$", "\\)", 1)
        resum = resum.replace("\\n ", "\n</p>\n<p>\n")

    while "$" in titol:
        titol = titol.replace("$", "\\(", 1)
        titol = titol.replace("$", "\\)", 1)

    xerrades_str = xerrades_str + "<p id=\"xerrada_" + xerrada + "\" class=\"entrada\">\n<b class=\"titol\">" + titol + "</b> (" + data + "). <br>\n"
    xerrades_str = xerrades_str + esdeveniment + ". <br>\n"
    xerrades_str = xerrades_str + "<em>" + lloc + "</em>. "

    if dades["transparencies"]:
        xerrades_str = xerrades_str + "<br>\n<a href=\"/xerrades/transparencies_" + xerrada + ".pdf\" target=\"_blank\" rel=\"noopener noreferrer\">Transparències</a>."
        
    xerrades_str = xerrades_str + "\n</p>"
    
    if "resum" in dades:
        xerrades_str = xerrades_str + "\n<details class=\"resum\">\n<summary>Resum</summary>\n<p>\n" + resum + "\n</p>\n</details>\n"



with open("public/index.html", "w") as index:
    index.write(html.replace("<!-- articles -->", articles_str).replace("<!-- conferencies -->", conferencies_str).replace("<!-- xerrades -->", xerrades_str).replace("<!-- edat -->", str(avui.year - naixement.year - ((avui.month, avui.day) < (naixement.month, naixement.day)))).replace("<!-- data -->", avui.strftime("%d/%m/%Y")))
    
    
    
    
    

    

############
###ANGLÈS###
############




with open("pre_index_en.html", "r") as pre_index:
    html = pre_index.read();


#Articles

articles_str = ""

for article in articles:
    with open("publicacions/" + article + ".json", "r") as fitxer_article:
        dades = json.loads(fitxer_article.read().replace('\\', '\\\\'))

    resum = dades["resum"]
    titol = dades["titol"]
    estat = dades["estat"] #preprint, submitted, accepted
    revista = dades["revista"]

    while "$" in resum:
        resum = resum.replace("$", "\\(", 1)
        resum = resum.replace("$", "\\)", 1)
    resum = resum.replace("\\n ", "\n</p>\n<p>\n")

    while "$" in titol:
        titol = titol.replace("$", "\\(", 1)
        titol = titol.replace("$", "\\)", 1)

    autors = ""
    if len(dades["autors"])>0:
        autors = "with " + dades["autors"][0]["nom"] + " " + dades["autors"][0]["cognom"]
        for i in range(1, len(dades["autors"])):
            if i == len(dades["autors"])-1:
                autors = autors + " and " + dades["autors"][i]["nom"] + " " + dades["autors"][i]["cognom"]
            else:
                autors = autors + ", " + dades["autors"][i]["nom"] + " " + dades["autors"][i]["cognom"]

    articles_str = articles_str + "<p id=\"article_" + article + "\" class=\"entrada\">\n<b class=\"titol\">" + titol + "</b>,<br>\n" + autors + ".<br>\n"

    if estat=="preprint":
        articles_str = articles_str + "<a href=\"https://arxiv.org/abs/" + dades["arxiv"] + "\" target=\"_blank\" rel=\"noopener noreferrer\">arXiv:" + dades["arxiv"] + "</a>."
    if estat=="submitted":
        articles_str = articles_str + "<em>Submitted.</em> <a href=\"https://arxiv.org/abs/" + dades["arxiv"] + "\" target=\"_blank\" rel=\"noopener noreferrer\">arXiv:" + dades["arxiv"] + "</a>."
    if estat=="accepted":
        articles_str = articles_str + "<em>" + revista + "</em> <a href=\"https://arxiv.org/abs/" + dades["arxiv"] + "\" target=\"_blank\" rel=\"noopener noreferrer\">arXiv:" + dades["arxiv"] + "</a>."
    
    if "conferencia" in dades:
        articles_str = articles_str + "<br>\n<a href=\"#conferencia_" + dades["conferencia"] + "\">Conference version</a>."

    articles_str = articles_str + "\n</p>\n<details class=\"resum\">\n<summary>Abstract</summary>\n<p>\n" + resum + "\n</p>\n</details>\n"




#Conferencies

conferencies_str = ""

for conferencia in conferencies:
    with open("conferencies/" + conferencia + ".json", "r") as fitxer_conferencia:
        dades = json.loads(fitxer_conferencia.read().replace('\\', '\\\\'))

    titol = dades["titol"]
    nom_conferencia = dades["conferencia"]

    while "$" in resum:
        resum = resum.replace("$", "\\(", 1)
        resum = resum.replace("$", "\\)", 1)
    resum = resum.replace("\\n ", "\n</p>\n<p>\n")

    while "$" in titol:
        titol = titol.replace("$", "\\(", 1)
        titol = titol.replace("$", "\\)", 1)

    autors = ""
    if len(dades["autors"])>0:
        autors = "with " + dades["autors"][0]["nom"] + " " + dades["autors"][0]["cognom"]
        for i in range(1, len(dades["autors"])):
            if i == len(dades["autors"])-1:
                autors = autors + " and " + dades["autors"][i]["nom"] + " " + dades["autors"][i]["cognom"]
            else:
                autors = autors + ", " + dades["autors"][i]["nom"] + " " + dades["autors"][i]["cognom"]

    conferencies_str = conferencies_str + "<p id=\"conferencia_" + conferencia + "\" class=\"entrada\">\n<b class=\"titol\">" + titol + "</b>,<br>\n" + autors + ".<br>\n<em>" + nom_conferencia + "</em><br>\n<a href=\"/conferencies/" + conferencia + ".pdf\" target=\"_blank\" rel=\"noopener noreferrer\">PDF</a>"
    
    if "acta" in dades:
        conferencies_str = conferencies_str + ",\n<a href=\"" + dades["acta"] + "\" target=\"_blank\" rel=\"noopener noreferrer\">proceedings</a>"
    
    if "article" in dades:
        conferencies_str = conferencies_str + ",\n<a href=\"#article_" + dades["article"] + "\">journal version</a>"
        
    conferencies_str = conferencies_str + ".\n</p>\n"
        
    if "resum" in dades:
        conferencies_str = conferencies_str + "<details class=\"resum\">\n<summary>Abstract</summary>\n<p>\n" + dades["resum"] + "\n</p>\n</details>\n"



#Xerrades

xerrades_str = ""

for xerrada in xerrades:
    with open("xerrades/" + xerrada + ".json", "r") as fitxer_xerrada:
        dades = json.loads(fitxer_xerrada.read().replace('\\', '\\\\'))

    titol = dades["titol"]
    data = dades["data"]
    esdeveniment = dades["esdeveniment"]
    lloc = dades["lloc_en"]

    while "$" in resum:
        resum = resum.replace("$", "\\(", 1)
        resum = resum.replace("$", "\\)", 1)
    resum = resum.replace("\\n ", "\n</p>\n<p>\n")

    while "$" in titol:
        titol = titol.replace("$", "\\(", 1)
        titol = titol.replace("$", "\\)", 1)

    xerrades_str = xerrades_str + "<p id=\"xerrada_" + xerrada + "\"  class=\"entrada\">\n<b class=\"titol\">" + titol + "</b> (" + data + "). <br>\n"
    xerrades_str = xerrades_str + esdeveniment + ". <br>\n"
    xerrades_str = xerrades_str + "<em>" + lloc + "</em>. "

    if dades["transparencies"]:
        xerrades_str = xerrades_str + "<br>\n<a href=\"/xerrades/transparencies_" + xerrada + ".pdf\" target=\"_blank\" rel=\"noopener noreferrer\">Slides</a>."
        
    xerrades_str = xerrades_str + "\n</p>"
    
    if "resum" in dades:
        xerrades_str = xerrades_str + "\n<details class=\"resum\">\n<summary>Abstract</summary>\n<p>\n" + dades["resum"] + "\n</p>\n</details>\n"


if not os.path.exists("public/en"):
    os.makedirs("public/en")

with open("public/en/index.html", "w") as index:
    index.write(html.replace("<!-- articles -->", articles_str).replace("<!-- conferencies -->", conferencies_str).replace("<!-- xerrades -->", xerrades_str).replace("<!-- edat -->", str(avui.year - naixement.year - ((avui.month, avui.day) < (naixement.month, naixement.day)))).replace("<!-- data -->", avui.strftime("%d/%m/%Y")))
